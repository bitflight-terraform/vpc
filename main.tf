####
#### Create a VPC in a region, with a number of AZ's
####

resource "aws_vpc" "main" {
    cidr_block = "${var.vpc_cidr}"
    enable_dns_support = "${var.enable_dns_support}"
    enable_dns_hostnames = "${var.enable_dns_hostnames}"
    
    tags {
        Name = "${var.resource_prefix}${var.vpc_name_suffix}"
    }
}


####
#### Gateways
####
resource "aws_internet_gateway" "main" {
    vpc_id = "${aws_vpc.main.id}"
}

resource "aws_nat_gateway" "main" {
  count             = "${var.make_private_subnets == "True" ? "1" : "0"}"
  allocation_id     = "${aws_eip.nat.id}"

  # Put it in the first subnet.
  subnet_id         = "${element(aws_subnet.public-subnets.*.id, count.index)}"
  
  depends_on        = ["aws_internet_gateway.main"]
  lifecycle {
    create_before_destroy = true
  }
}


### Only create one NAT for now
resource "aws_eip" "nat" {
  count             = "${var.make_private_subnets == "True" ? "1" : "0"}"

  vpc = true

  lifecycle {
    create_before_destroy = true
  }
}

####
#### Subnets
####


resource "aws_subnet" "public-subnets" {
  count             = "${var.az_count}"
  cidr_block        = "${cidrsubnet(aws_vpc.main.cidr_block, 8, count.index)}"
  availability_zone = "${data.aws_availability_zones.all.names[count.index]}"
  vpc_id            = "${aws_vpc.main.id}"
  map_public_ip_on_launch = true
  tags {
        Name = "${var.resource_prefix}-public-subnet"
        "Terraform" = "true"
    }
}

resource "aws_subnet" "private-subnets" {
  count = "${var.make_private_subnets == "True" ? var.az_count : "0"}"
  cidr_block        = "${cidrsubnet(aws_vpc.main.cidr_block, 8, count.index)}"
  availability_zone = "${data.aws_availability_zones.all.names[count.index]}"
  vpc_id            = "${aws_vpc.main.id}"
  map_public_ip_on_launch = false
  tags {
        Name = "${var.resource_prefix}-private-subnet"
        "Terraform" = "true"
    }
}

####
#### Route Table
####

#  PUBLIC
resource "aws_route_table" "public" {
    vpc_id = "${aws_vpc.main.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.main.id}"
    }

    tags {
        Name = "${var.resource_prefix}-public-route-table"
        "Terraform" = "true"
    }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route_table_association" "public" {
    count = "${var.az_count}"
    subnet_id = "${element(aws_subnet.public-subnets.*.id, count.index)}"
    route_table_id = "${aws_route_table.public.id}"
}

#  PRIVATE
resource "aws_route_table" "private" {
    count = "${var.make_private_subnets == "True" ? "1" : "0"}"
    vpc_id = "${aws_vpc.main.id}"

    route {
      cidr_block     = "0.0.0.0/0"
      nat_gateway_id = "${element(aws_nat_gateway.main.*.id, count.index)}"
    }

    tags {
        Name = "${var.resource_prefix}-private-route-table"
        "Terraform" = "true"
    }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route_table_association" "private" {
    count = "${var.make_private_subnets == "True" ? var.az_count : "0"}"
    subnet_id = "${element(aws_subnet.private-subnets.*.id, count.index)}"
    route_table_id = "${aws_route_table.private.id}"
}


####
#### Modules
####

module "route53" {
  source            = "./route53"
  domainname        = "${var.domainname}"
  vpc_id            = "${aws_vpc.main.id}"
  internal_dns_zone = "${var.internal_dns_zone}"
}

module "dhcp" {
  source          = "./dhcp"
  domainname      = "${module.route53.domainname}"
  vpc_id          = "${aws_vpc.main.id}"
  aws_region      = "${var.aws_region}"
}