#Module example

###Configures DHCP to use a route53 internal zone for lookups
```
module "dhcp" {
  source          = "./dhcp"
  domainname      = "${module.route53.domainname}"
  vpc_id          = "${aws_vpc.main.id}"
}```