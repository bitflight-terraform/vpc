variable "domainname" {}
variable "aws_region" {}
variable "vpc_id" {}

resource "aws_vpc_dhcp_options" "dns_resolver" {
  domain_name_servers   = ["AmazonProvidedDNS"]
  domain_name           = "internal.${var.domainname} ${var.aws_region}.compute.internal"
  tags {
    Name                = "internal.${var.domainname}"
    Terraform           = "true"
  }
}

resource "aws_vpc_dhcp_options_association" "dns_resolver" {
  vpc_id                = "${var.vpc_id}"
  dhcp_options_id       = "${aws_vpc_dhcp_options.dns_resolver.id}"
}
output "domainname" {
  value = "${var.domainname}"
}