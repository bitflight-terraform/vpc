####
#### Variables
####

variable "domainname" {
  description = "which domain name to use when creating a zone"
}
variable "vpc_id" {
  description = "which zone id to add it to"
}
variable "prefix" {
  default = "internal."
}
variable "resource_prefix" {
  default = "dev"
}

variable "internal_dns_zone" { default = "_none_" }


####
#### Resources
####

resource "aws_route53_zone" "main" {
  name = "${var.prefix}${var.domainname}"
  vpc_id = "${var.vpc_id}"
  comment = "${var.resource_prefix}"
}


####
#### Outputs
####

output "zone_id" {
  value = "${aws_route53_zone.main.zone_id}"
}
output "name" {
  value = "${aws_route53_zone.main.name}"
}
output "domainname" {
  value = "${var.domainname}"
}
output "vpc_id" {
  value = "${var.vpc_id}"
}