vpc module

To modify this module
> git add .
> git commit -m "Made some changes to frontend-app"
> git push origin master
> git tag -a "v0.0.1" -m "First release of <module-name>"
> git push --follow-tags

Examples of using this module:

module "vpc" {
  #Required
  source = "git::https://gitlab.com/bitflight-terraform/vpc.git//frontend-app?ref=v0.0.1"
  num_az = 2

  #Optional
  make_private_subnets = "False"
}

module "vpc_with_private" {
  #Required
  source = "git::https://gitlab.com/bitflight-terraform/vpc.git//frontend-app?ref=v0.0.1"
  num_az = 2

  #Optional
  #Creates equal number of subnets that use a nat gateway
  make_private_subnets = "True"
}

module "vpc_with_dhcp_resolution" {
  source = "git::https://gitlab.com/bitflight-terraform/vpc.git//frontend-app?ref=v0.0.1"
  num_az = 2

  #Optional

}