variable "aws_region" {
  description = "which aws region should this vpc be created in?"
  default = "ap-southeast-2"
}
variable "vpc_cidr" {
  description = "what CIDR range should this VPC use"
  default = "10.80.0.0/16"
}
variable "resource_prefix" {
  description = "use this to label the environment such as dev, prod, staging"
  default = "dev"
}

variable "vpc_name_suffix" {
  description = "vpc is named <resource_prefix><vpc_name_suffix>"
  default = "-vpc"
}

variable "az_count" {
  description = "Number of AZ's to create subnets in"
  default = "1"
}

variable "make_private_subnets" { 
  description = "Make private subnets equal to the public subnets, and create an internet gateway True or False"
  default = "False"
}


variable "enable_dns_support" {default = true}
variable "enable_dns_hostnames" {default = true}

####
#### Use one of the below, If you want to make a new zone, just provide a name
#### otherwise provide an ID of an existing zone, set the other one to "_none_"
####

variable "domainname" {
  description = "Use this domain for internal dns records"
  default = "_none_"
}

variable "internal_dns_zone" {
  description = "Use this zone for internal dns records"
  default = "_none_"
}

data "aws_availability_zones" "all" {}