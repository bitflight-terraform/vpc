output "vpc_id" {
  value = "${aws_vpc.main.id}"
}

output "internal_zone_id" {
  value = "${module.route53.zone_id}"
}

output "internal_zone_name" {
  value = "${module.route53.name}"
}

output "subnet_ids" {
   value = "${aws_subnet.public-subnets.*.id}"
}